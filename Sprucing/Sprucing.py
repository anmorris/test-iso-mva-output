from Moore import Options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.calorimeter_reconstruction import make_digits
from PyConf.application import metainfo_repos
from Moore.persistence.hlt2_tistos import list_of_full_stream_lines
from Hlt2Conf.sprucing_settings.fixed_line_configs import lines_for_TISTOS_BW_Aug2023 as lines_for_TISTOS
# from Hlt2Conf.lines.semileptonic.spruce_semileptonic import spruce_xib0toxicplustaunu_xicplustopkpi_tautoenunu_line as line_to_run
from Hlt2Conf.lines.semileptonic import sprucing_lines # use whole module to account for overlap
from Moore.streams import DETECTORS, Stream, Streams
#from Moore.persistence.truth_matching import truth_match_lines
#from RecoConf.reco_objects_for_spruce import reconstruction as reconstruction_for_spruce, upfront_reconstruction as upfront_reconstruction_for_spruce


#test_lines = [builder() for name, builder in sprucing_lines.items() if name in (
#        "SpruceSLB_LbToLcMuNu_LcToPKPi",
#        )]


def make_streams():
    streams = [
    Stream(
            "sprucing",
            lines=[builder() for name, builder in sprucing_lines.items() if name in (
            "SpruceSLB_LbToLcMuNu_LcToPKPi",
            )],
            # lines=[line_to_run()] # single line
            detectors=[]) # usual case - no detector raw banks
            # detectors=DETECTORS) # if persisting detector raw banks
    ]
    return Streams(streams=streams)

#reconstruction_for_spruce.global_bind(
#                simulation=True)
#upfront_reconstruction_for_spruce.global_bind(
#                simulation=True)
def main(options: Options):
    make_digits.global_bind(calo_raw_bank=False)
    
    public_tools = [stateProvider_with_simplified_geom()]
    with reconstruction.bind(
            from_file=True,
            spruce=True), list_of_full_stream_lines.bind(lines=lines_for_TISTOS):#, truth_match_lines(lines=test_lines):
        config = run_moore(options, make_streams, public_tools, exclude_incompatible=True)
    return config
    
