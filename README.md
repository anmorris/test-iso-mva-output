# Testing the isolation MVA on MC

This repo tests the isolation MVA for the SLB group on MC by doing the following:

With a DIGI file:

 * Run HLT1
 * Run HLT2
 * Run Sprucing
   * This will output the iso MVA monitoring histogram
 * Run DaVinci

At this point the output will be handed off to Veronica to run the xgboost algo on it, and this can be compared to the monitoring histogram.

Instructions for how to run each stage are given in a README in each folder.
