###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running a set of HLT2 lines -> mdf to calculate BW.

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_bandwidth_example.py
    file_size = ls -lh --si hlt2_bandwidth_example.mdf
    b/w (GB/s) = input rate (1140 kHz for this min bias) x file_size (MB) / options.evt_max
"""

from Moore import options, run_moore
# Imports for 2024-like no-UT reconstruction scenario aligned with hlt2_pp_thor_without_UT
from RecoConf.global_tools import (
    stateProvider_with_simplified_geom,
    trackMasterExtrapolator_with_simplified_geom,
)
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.hlt2_global_reco import (
    reconstruction as hlt2_reconstruction,
    make_light_reco_pr_kf_without_UT,
)
from RecoConf.ttrack_selections_reco import make_ttrack_reco
from Moore.streams import DETECTORS, Stream, Streams
from Moore.persistence.truth_matching import truth_match_lines

from Hlt2Conf.lines.topological_b import all_lines as all_lines_topo
from Hlt2Conf.lines.semileptonic import all_lines as all_lines_SLB# use whole module to account for overlap


options.conddb_tag = "sim-20231017-vc-md100"
options.dddb_tag = "dddb-20231017"
options.data_type = "Upgrade"
options.simulation = True
options.evt_max = -1
options.scheduler_legacy_mode = False
options.input_manifest_file = "../HLT1/HLT1.tck.json"
options.input_files = ["../HLT1/HLT1.dst"]
options.input_type = "ROOT"
options.input_raw_format = 0.5
options.output_file = 'HLT2.dst'
options.output_type = 'ROOT'
options.output_manifest_file = "HLT2.tck.json"
options.histo_file = "monitoring_histos.root"

#print([builder() for builder in all_lines_SLB.values()])
#exit()
#test_lines = [builder() for builder in all_lines_SLB.values()] + [builder() for builder in all_lines_topo.values()]

def make_streams():
    streams = [
        Stream(
            "HLT2",
            lines = [builder() for builder in all_lines_SLB.values()] + [builder() for builder in all_lines_topo.values()]
                  ,
            #lines=[builder() for name, builder in all_lines_SLB.items() if ("BcToBsPi" in name or "BcToBsK" in name) and "BsToDs" in name],
            #lines=[builder() for linename, builder in all_lines_SLB.items() if "Hlt2SLB_BuToD0TauNu_D0ToK3Pi_TauToPiPiPiNu" in linename], # whole module
            routing_bit=85, # some dummy value != 94 or 95
#            lines=[Hlt2SLB_BcToBsPi_BsToDsPi(), Hlt2SLB_BcToBsK_BsToDsPi(), Hlt2SLB_BcToBsPi_BsToDsK(), Hlt2SLB_BcToBsK_BsToDsK()],# single line
            #detectors=[]) # Turbo case - no detector raw banks
             detectors=DETECTORS) # if persisting detector raw banks i.e. Full or TurCal lines
    ]
    return Streams(streams=streams)

public_tools = [
    trackMasterExtrapolator_with_simplified_geom(),
    stateProvider_with_simplified_geom(),
]
with reconstruction.bind(from_file=False), make_light_reco_pr_kf_without_UT.bind(
    skipRich=False, skipCalo=False, skipMuon=False
), make_ttrack_reco.bind(skipCalo=False), hlt2_reconstruction.bind(
    make_reconstruction=make_light_reco_pr_kf_without_UT
):#, truth_match_lines(lines=test_lines):
    config = run_moore(options, make_streams, public_tools)

