To get this DST I ran:

`./../../Moore/run lbexec HLT1.py:main HLT1_options.yaml |& tee HLT1.log`

If you see lots of Plume warnings about no bank found, then see here:

https://mattermost.web.cern.ch/lhcb/pl/atngfp33nfguibsk8ad6bzciwo
