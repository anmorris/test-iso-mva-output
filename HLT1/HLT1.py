###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore.config import allen_control_flow
from Moore import Options
from RecoConf.hlt1_muonid import make_muon_hits
from RecoConf.hlt1_allen import allen_gaudi_config as allen_sequence
from PyConf.application import configure_input, configure
from PyConf.control_flow import CompositeNode, NodeLogic

#need to bind geometry version to 3 for muon hits
print(f"file {__file__} is open!")
make_muon_hits.global_bind(geometry_version=3)

def main(options: Options):
  with allen_sequence.bind(sequence="hlt1_pp_matching_no_ut"):
    config = configure_input(options)
    allen_node = allen_control_flow(options)
    top_cf_node = CompositeNode('MooreAllenWithLogger', combine_logic=NodeLogic.LAZY_AND, children=[allen_node],force_order=True)
    config.update(configure(options, top_cf_node))
  return config
